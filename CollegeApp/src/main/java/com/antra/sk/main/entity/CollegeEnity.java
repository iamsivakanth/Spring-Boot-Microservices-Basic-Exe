package com.antra.sk.main.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "COLLEGE_TAB")
public class CollegeEnity {

	@Id
	private Integer collegeid;
	private String collegename;
	private String collegecity;
}