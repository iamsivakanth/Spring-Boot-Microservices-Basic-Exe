package com.antra.sk.main.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.antra.sk.main.entity.StudentEntity;
import com.antra.sk.main.service.StudentServiceInter;

@RestController
@RequestMapping("/student")
public class StudentRestController {

	@Autowired
	private StudentServiceInter service;

	@PostMapping("/add")
	public ResponseEntity<?> addStudent(@RequestBody StudentEntity entity) {
		return new ResponseEntity<>(service.insert(entity), HttpStatus.CREATED);
	}

	@GetMapping("/view")
	public ResponseEntity<?> getStudents() {
		return ResponseEntity.ok(service.getStudents());
	}

	@GetMapping("/view/{id}")
	public ResponseEntity<?> getStudents(@PathVariable(value = "id") Integer cid) {
		return ResponseEntity.ok(service.getStudentsByCollegeId(cid));
	}
}