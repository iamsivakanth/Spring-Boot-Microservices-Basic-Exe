package com.antra.sk.main.service;

import java.util.List;

import com.antra.sk.main.entity.CollegeEnity;
import com.antra.sk.main.model.ResponseSender;

public interface CollegeServiceInter {

	CollegeEnity insert(CollegeEnity enity);

	ResponseSender getDetails(Integer cid);

	List<?> fetchCollege(Integer cid);

	List<String> fetchStudentNames(Integer cid);
}