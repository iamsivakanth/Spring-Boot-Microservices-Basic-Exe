package com.antra.sk.main.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.antra.sk.main.entity.CollegeEnity;

@Repository
public interface CollegeRepository extends JpaRepository<CollegeEnity, Integer> {
}