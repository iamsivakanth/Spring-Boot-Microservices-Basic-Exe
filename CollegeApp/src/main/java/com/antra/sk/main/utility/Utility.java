package com.antra.sk.main.utility;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.antra.sk.main.model.StudentEntity;
import com.google.gson.Gson;

@Component
public class Utility {

	@Autowired
	RestTemplate template;

	public  List<StudentEntity> getList(Integer cid) {
		List list = template.getForObject("http://STUDENT-SERVICE/student/view/" + cid, List.class);

		List<StudentEntity> entities = new ArrayList<>();

		for (int i = 0; i < list.size(); i++) {
			String json = new Gson().toJson(list.get(i));
			StudentEntity entity = new Gson().fromJson(json, StudentEntity.class);
			entities.add(entity);
		}
		return entities;
	}
}