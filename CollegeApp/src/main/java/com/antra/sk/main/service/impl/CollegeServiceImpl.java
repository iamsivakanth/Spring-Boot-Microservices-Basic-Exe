package com.antra.sk.main.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.antra.sk.main.dao.CollegeRepository;
import com.antra.sk.main.entity.CollegeEnity;
import com.antra.sk.main.model.ResponseSender;
import com.antra.sk.main.service.CollegeServiceInter;
import com.antra.sk.main.utility.Utility;

@Service
public class CollegeServiceImpl implements CollegeServiceInter {

	@Autowired
	private CollegeRepository dao;

	@Autowired
	Utility utility;

	@Override
	public CollegeEnity insert(CollegeEnity enity) {
		return dao.save(enity);
	}

	@Override
	public ResponseSender getDetails(Integer cid) {
		ResponseSender responseSender = ResponseSender.build();
		CollegeEnity college = dao.findById(cid).get();

		responseSender.setCollege(college);
		responseSender.setStudents(utility.getList(cid));
		return responseSender;
	}

	@Override
	public List<?> fetchCollege(Integer cid) {
		return dao.findAll();
	}

	@Override
	public List<String> fetchStudentNames(Integer cid) {
		return utility.getList(cid).stream().map(t -> t.getStuname()).collect(Collectors.toList());
	}
}