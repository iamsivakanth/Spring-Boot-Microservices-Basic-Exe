package com.antra.sk.main.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.antra.sk.main.dao.StudentRepository;
import com.antra.sk.main.entity.StudentEntity;
import com.antra.sk.main.service.StudentServiceInter;

@Service
public class StudentServiceImpl implements StudentServiceInter {

	@Autowired
	private StudentRepository dao;

	@Override
	public StudentEntity insert(StudentEntity entity) {
		return dao.save(entity);
	}

	@Override
	public List<StudentEntity> getStudents() {
		return dao.findAll();
	}

	@Override
	public List<StudentEntity> getStudentsByCollegeId(Integer cid) {
		return dao.findByCollegeid(cid);
	}
}