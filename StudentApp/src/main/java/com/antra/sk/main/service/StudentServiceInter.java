package com.antra.sk.main.service;

import java.util.List;

import com.antra.sk.main.entity.StudentEntity;

public interface StudentServiceInter {

	StudentEntity insert(StudentEntity entity);

	List<StudentEntity> getStudents();

	List<StudentEntity> getStudentsByCollegeId(Integer cid);
}