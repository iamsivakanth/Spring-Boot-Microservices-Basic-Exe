package com.antra.sk.main.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.antra.sk.main.entity.CollegeEnity;
import com.antra.sk.main.service.CollegeServiceInter;

@RestController
@RequestMapping("/college")
public class CollegeController {

	@Autowired
	private CollegeServiceInter service;

	@ResponseStatus(value = HttpStatus.CREATED)
	@PostMapping("/add")
	public CollegeEnity addCollege(@RequestBody CollegeEnity enity) {
		return service.insert(enity);
	}

	@GetMapping("/getcollege/{cid}")
	public ResponseEntity<?> getColleges(@PathVariable Integer cid) {
		return ResponseEntity.ok(service.getDetails(cid));
	}

	@GetMapping("/getcol/{cid}")
	public ResponseEntity<?> getCollegesInfo(@PathVariable Integer cid) {
		return ResponseEntity.ok(service.fetchCollege(cid));
	}

	@ResponseStatus(value = HttpStatus.OK)
	@GetMapping("/getnames/{cid}")
	public List<String> getStuNames(@PathVariable Integer cid) {
		return service.fetchStudentNames(cid);
	}
}