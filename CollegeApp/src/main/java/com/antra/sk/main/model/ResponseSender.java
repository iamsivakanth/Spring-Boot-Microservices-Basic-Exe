package com.antra.sk.main.model;

import java.util.List;

import com.antra.sk.main.entity.CollegeEnity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor(staticName = "build")
public class ResponseSender {

	private CollegeEnity college;
	List<StudentEntity> students;
}
